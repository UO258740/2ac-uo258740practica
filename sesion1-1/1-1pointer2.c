#include <stdio.h>
#include <stdlib.h>

void f(double d)
{
	d = 4;
}

int main(int argc, char* argv[])
{
	double * pd;
	double d = 4;
	pd = &d;
	f(d);
	printf("%f\n", d);
	return 0;
}

