main()
{
  /* Keyboard interupt vector address */
  int _far *p = (int _far *)0x00024;

  *p = 0xFFF0; /* Write offset in interrupt vector */
  p++;
  *p = 0xF000; /* Write segment in interrupt vector */
}
