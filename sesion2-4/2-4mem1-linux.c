int main()
{
  /* Interupt vector address */
	int *p = (int *)0x20;

	*p = 0x0000; /* Write offset in interrupt vector */
	p++;
	*p = 0x0000; /* Write segment in interrupt vector */
}
