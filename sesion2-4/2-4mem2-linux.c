int main()
{
  /* Interupt vector address */
  int *p = (int *)0x20;

  *p = 0xFFFF; /* Write offset in interrupt vector */
  p++;
  *p = 0x0; /* Write segment in interrupt vector */
}
