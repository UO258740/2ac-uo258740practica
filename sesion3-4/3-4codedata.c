// This source file must be compiled with the following command:
//   gcc -Wall 3-4codedata.c 3 -o 3-4codedata -lmem
#include <stdio.h>
#include <stdlib.h>
#include <atc/linmem.h>


int main(int argc, char *argv[])
{
	unsigned int  kernelcodemin, kerneldatamax;
	unsigned int flag;
	unsigned int address;
	unsigned int codeAddress = 0, dataAddress = 0;
	const unsigned int CODE_FLAGS = 0x161;
	const unsigned int DATA_FLAGS = 0x163;
	const unsigned int FLAG_MASK = 0xFFF;
	const unsigned int OS_BASE_ADDR = 0xC0000000;
	const unsigned int PAGE_SIZE = 1 << 12;

	if (argc != 3)
	{
		fprintf(stderr, "Wrong number of arguments. Usage: %s code_lowest_addr data_highest_addr\n",
			argv[0]);
		 return -1;
	}

	kernelcodemin = strtoul(argv[1], NULL, 16) | OS_BASE_ADDR;
	kerneldatamax = strtoul(argv[2], NULL, 16) | OS_BASE_ADDR;
	if (kerneldatamax < kernelcodemin)
	{
		fprintf(stderr, "code_lowest_addr should lower than data_highest_addr\n");
		return -1;
	}

	// Look for an address in the kernel code present in physical memory
	for (address = kernelcodemin; address <= kerneldatamax; address += PAGE_SIZE)
	{
		if (get_pte((void *)address, &flag) == 0)
		{
			if ( (flag & FLAG_MASK) == CODE_FLAGS)
			{
				codeAddress = address;
				break;
			}
		}
	}

	// Look for an address in the kernel data present in physical memory
	for (address += PAGE_SIZE; address <= kerneldatamax; address += PAGE_SIZE)
	{
		if (get_pte((void *)address, &flag) == 0)
		{
			if ( (flag & FLAG_MASK) == DATA_FLAGS)
			{
				dataAddress = address;
				break;
			}
		}
	}

	if (codeAddress)
		printf("\nKernel memory address 1:\t %.8X\n", codeAddress);
	else
		printf("\nKernel memory address 1:\t No address\n");
	if (dataAddress)
		printf("\nKernel memory address 2:\t %.8X \n\n", dataAddress);
	else
		printf("\nKernel memory address 2:\t No address\n");
}
